package net.xdclass.mapper;

import net.xdclass.model.ProductOrderDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.xdclass.model.ProductOrderItemDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 二当家小D
 * @since 2021-02-22
 */
public interface ProductOrderMapper extends BaseMapper<ProductOrderDO> {

    /**
     * 批量插入
     * @param list
     */
    void insertBatch(@Param("orderItemList") List<ProductOrderItemDO> list);

    /**
     * 更新订单状态
     * @param outTradeNo
     * @param newState
     * @param oldState
     */
    void updateOrderPayState(@Param("outTradeNo") String outTradeNo,@Param("newState") String newState, @Param("oldState") String oldState);
}
