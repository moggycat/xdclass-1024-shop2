package net.xdclass.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;

public class AlipayConfig {

    /**
     * 支付宝网关地址  TODO
     */
    public static final  String PAY_GATEWAY="https://openapi.alipaydev.com/gateway.do";


    /**
     * 支付宝 APPID TODO
     */
    public static final  String APPID="2021000117690814";

    /**
     * 应用私钥 TODO
     */
    public static final String APP_PRI_KEY =  "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCFcAJrCmA9ozDHM8DfpPv2yKf/9SgenbdYwqhVSlQj0C5MGqrt99Eqb2f1edl11MwXKirJtpAcPiXkKJSDF4PmI+xxvXs77kaeeYGn5YO+s+1h4CYDUCnKCmLr7qKxfpVdyII2DKrLZ2lx4216BrbqZjuQKN6kKkYWGzzLmO24Qy/dK36HAIHidARHf9wdOp425HS8BJAQQBJX5JIPzAhwstkft/h/ifyXdopuLd/+A1pX86eyANEcggm9/Wh5Ncwrjd5MuN/tEpMA1FHiVl1SF5Qxu+0SHgHMEGFRmJDH2JXOZhgb+PdB6h9FXWg6SV9oH2g0AYsW75DJbeUT61KfAgMBAAECggEAVOVOpIuVb6HEBvkGXUO6iuhe+pofTFtLq00tjBYeMirhScottflVOopmuecsU6tvrc1GK8/bIBzqPp8S3dS4NlPkuviyzw5qH6lutENx7oLLJNpXVQRLkh/JkBHjsMm4flJzha/Jel/w1mS74kcjWT5x5uYZwJikvqvVojNKC1DF3hV4ierKx7aZ8bshTGAM47GFgl+K//chexIYd3InP2oh0ZTW0qb4lOuLQ7XYkhqWAjJMEhq60929nNvUEuEJUUCzu5a93xCvsLhYTqOAhPk4JI09GBeNFkhf+rTz8D155xoL/ieNWTQOoRMeB4AvoejJZi36i1JmAmszlGeuAQKBgQDXHA1S3mWyInQBqXuJAG3OKHq0LbyX5eYsSwK/tIU3xWa/dva4+tiQ7p1/0Sr7MTFmY3vLsJVzvENYf6MuvMDpHOr+Ep4+9WJnLdMajMSQvphCnyl1+LcB5FWzpHIdYxDg5poKUQb9o3/mJDrULKMiToxdZdUrxPraXIcjTWV/mQKBgQCezYYpG7csLHFc8TDOcwq99UZUxKwWOnLXZlojPlSTdY3o9gZalcypLgdoPFdmB0AfZP6MzR3gVOUw+2WETek4aMEaAwc36zighMKu8QC2IGeQQpNv/lv3LPOEmGRx/ItUBI4+wvdn1Wr2AOc//1FrD5nZZks2sSoLwwvBMECm9wKBgAaezWYshBK8breSYMPxqG983XHoKnK+V5wrXtVJmAQEZkokPimpRrsHLP+/D0SV2gBdtXSqZZ8fPy9dFkec9Y73HeBd8FLduH6WcXXSuUX/J659OYhw9CjxvxV9TSnMX7ucPUduAPoTax9JJCpdiVPH+Rfyx2HGeTX5PQuAGUyZAoGAbuIxWIjquOR7b5tWKgF4MbWPG2eMhaTgRXlJXSyLn79IjK8lNqE8m7DxKR5hJHPRmVpKx4P95YNB2ZbghqZkP6gdlY0hwDD1hFfPHJpZldJAniFuk5e3HXt8ukwybskZfFwF+trmJ9GEcyukTTJ45lmpYnyj7hd8tWPG1Gld63cCgYEAoorwjun7QyMyBS+sk+QrRciksHaObbPmXbLLZdZQscvq3LtsTc2xPRstJrHSC0kgFn33+cgl2gLkCDTd0OD/Qtzg/Xmc1Z7qAD0rzUwprpUD4UzzNQIemjm/RNYx9wwOcCUN1zuZ37/HCheSvYhOr9thxGFSHgurnJi9TDqZYTM=";

    /**
     * 支付宝公钥 TODO
     */
    public static final String ALIPAY_PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhXACawpgPaMwxzPA36T79sin//UoHp23WMKoVUpUI9AuTBqq7ffRKm9n9XnZddTMFyoqybaQHD4l5CiUgxeD5iPscb17O+5GnnmBp+WDvrPtYeAmA1Apygpi6+6isX6VXciCNgyqy2dpceNtega26mY7kCjepCpGFhs8y5jtuEMv3St+hwCB4nQER3/cHTqeNuR0vASQEEASV+SSD8wIcLLZH7f4f4n8l3aKbi3f/gNaV/OnsgDRHIIJvf1oeTXMK43eTLjf7RKTANRR4lZdUheUMbvtEh4BzBBhUZiQx9iVzmYYG/j3QeofRV1oOklfaB9oNAGLFu+QyW3lE+tSnwIDAQAB";

    /**
     * 签名类型
     */
    public static final  String SIGN_TYPE="RSA2";


    /**
     * 字符编码
     */
    public static final  String CHARSET="UTF-8";


    /**
     * 返回参数格式
     */
    public static final  String FORMAT="json";


    /**
     * 构造函数私有化
     */
    private AlipayConfig(){

    }


    /**
     * 防止指令重排
     */
    private volatile static AlipayClient instance = null;


    /**
     * 单例模式获取, 双重锁校验
     * @return
     */
    public static AlipayClient getInstance(){

        if(instance==null){
            synchronized (AlipayConfig.class){
                if(instance == null){
                    instance = new DefaultAlipayClient(PAY_GATEWAY,APPID,APP_PRI_KEY,FORMAT,CHARSET,ALIPAY_PUB_KEY,SIGN_TYPE);
                }
            }
        }
        return instance;
    }

}
